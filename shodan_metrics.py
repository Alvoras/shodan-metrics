#!/usr/bin/env python3

import subprocess
import json
from datetime import datetime
import time

import schedule
import yaml


class Digest:
    def __init__(self, logfile):
        self.logs = []
        self.timestamp = datetime.now().isoformat()
        self.logfile = logfile

    def add(self, log):
        log.timestamp = self.timestamp
        self.logs.append(log)

    def get_json_logs(self):
        return [l.to_json() for l in self.logs]

    def write_to_log_file(self):
        with open(self.logfile, "a+") as f:
            for log in self.get_json_logs():
                f.write(f"{log}\n")

        print(f"[+] Wrote {len(self.logs)} results to {self.logfile}")


class Log:
    def __init__(self, count, query, proto, tags):
        self.count = count
        self.query = query
        self.tags = tags
        self.proto = proto
        self.timestamp = ""

    def to_dict(self):
        return {
            "count": self.count,
            "tags": self.tags,
            "query": self.query,
            "techno": self.proto,
            "timestamp": self.timestamp,
        }

    def to_json(self):
        return json.dumps(self.to_dict())


def run():
    print("[+] Started job")

    # Reload the lookup file before running
    with open("lookup.yml", "r") as f:
        try:
            lookup = yaml.safe_load(f)
        except yaml.YAMLError as e:
            print(f"[-] {e}")
            return

    digest = Digest("logs.jsonl")

    for (proto, lookup) in lookup.items():
        cmd = f"shodan count \"{lookup['query']}\""
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out, err = p.communicate()

        if err:
            print(err)
            continue
        if out:
            log = Log(int(out), lookup["query"], proto, lookup["tags"])
            digest.add(log)

    digest.write_to_log_file()


if __name__ == '__main__':
    print("[+] Scheduler started")
    schedule.every().day.at("01:00").do(run)

    while True:
        schedule.run_pending()
        time.sleep(10)


